import React, { Component } from 'react';
import Logo from '../componets/Logo';
import EvtolNav from '../componets/EvtolNav';
import BasicNav from '../componets/BasicNav';
import EvtolHelpPopUp from './../componets/EvtolHelpPopUp';

class Evtol extends Component {
  constructor(props){
    super(props);
    this.state = { showPopup: false };
    this.state = {
      image_url : [
        "./images/evtol/slide-03-VTOL-solutions.jpg",
        "./images/evtol/slide-04---overview.jpg",
        "./images/evtol/slide 05-overview.jpg",
        "./images/evtol/slide 6-Cockpit controls.jpg",
        "./images/evtol/slide 7-electromechanical Actuation.jpg",
        "./images/evtol/slide 8-electromechanical Actuation.jpg",
        "./images/evtol/slide 9-Sensors.jpg",
        "./images/evtol/slide 10-isolators.jpg",
        "./images/evtol/Slide 11-Cabin Comfort.jpg",
        "./images/evtol/slide 12 - blade attachment.jpg",
        "./images/evtol/Slide 13-Gimbaled rotor systems.jpg",
        "./images/evtol/Slide 14-thermal management materials.jpg",
        "./images/evtol/Slide 15- beond concept.jpg",
      ],
      img : 0
    }
  }
  picture = (e) => {
    this.setState({
      img : e
    })
  }

  state = {
    isMobileMenu: false
}
 
togglePopup() {  
  this.setState({  
       showPopup: !this.state.showPopup  
  });  
} 

mobileMenu = () => {
  const { isMobileMenu } = this.state
  this.setState({ isMobileMenu: !isMobileMenu })
}

  render() {
    const { isMobileMenu } = this.state;
    return (
      <div className={ isMobileMenu ? "evto flex flex-wrap w-full h-full bg-cover relative mobile-active" : "tip-tail flex h-full flex-wrap w-full bg-cover relative"}>
      <div className="w-full flex h-24 justify-between relative z-10">
        <div className="logo p-4 lg:flex items-center h-24">
          <Logo/>
          <h2 className="text-black text-xl lg:text-2xl lg:text-5xl italic font-medium leading-none pt-4 lg:pt-0 lg:pl-8">Electric / Hybrid Vertical Take-Off & Landing Solutions</h2>
        </div>
        <div className="content--inner content"></div>
        <div className="header-right mr-12">
          <a className="hidden button relative lg:inline-flex items-center px-8 button-border-slant text-red-500 text-4xl font-semibold m-3" onClick={this.togglePopup.bind(this)}>Help</a>
          {this.state.showPopup ?  
            <EvtolHelpPopUp  
              closePopup={this.togglePopup.bind(this)}  
            />
            : null  
          } 
          <button id="js-mobile" className="uppercase mr-2 menu-toggle menu-toggle--collapse inline-block lg:hidden flex items-center" aria-controls="primary-menu" aria-expanded="false" type="button" onClick={ this.mobileMenu }>
            <span className="menu-toggle-box ml-2">
            <span className="menu-toggle-inner"></span>
            </span>
          </button>
      </div>
    </div> 
        <div className="mro__wrap w-full lg:flex">
            <div className="lg:w-1/4">
                 <EvtolNav pic={this.picture} />
            </div>
            <div className="lg:w-3/4">
              <img src={this.state.image_url[this.state.img]} alt="Evtol"/>
            </div>
        </div>
        <div className="hidden lg:flex bottom-nav fixed lg:bottom-0 right-0 w-full lg:w-1/2 z-30 items-end">
          <div className="flex justify-end w-full relative">
            <BasicNav/>
          </div>
        </div>
      </div>
    );
  }
}
export default Evtol; 