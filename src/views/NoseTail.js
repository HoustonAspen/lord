import React, { Component } from 'react';
import Logo from '../componets/Logo';
import FixedLegend from '../componets/FixedLegend';
import FixedNav from '../componets/FixedNav';
import MarkerBtn from '../svg/marker';
import HelpPopup from './../componets/HelpPopup';
import FixedBody from './../componets/FixedBody';
import FixedNose from './../componets/FixedNose';
import FixedWing from './../componets/FixedWing';
import ShowAllFixed from '../componets/ShowAllFixed';

class TipTail extends Component { 
  constructor(props){  
    super(props);  
    this.state = { 
      cls : "active",
      showPopup: false,
      isFixedBody: false,
      isFixedNose: false,
      isFixedWing: false,
      isMobileMenu: false,
      isShowAllFixed: false 
    };  
  }  
  togglePopup() {  
    this.setState({  
         showPopup: !this.state.showPopup  
    });  
  } 

  mobileMenu = () => {
    const { isMobileMenu } = this.state
    this.setState({ isMobileMenu: !isMobileMenu })
  }

  showAllFixed = () => {
    const { isShowAllFixed } = this.state
    this.setState({ isShowAllFixed: !isShowAllFixed })
    this.popup_fixed_check();
  }

  handleToggleFixedBody = () => {
     const { isFixedBody } = this.state
     this.setState({ isFixedBody: !isFixedBody })
     this.checkToggleFixedBody();
  }
  
  handleToggleFixedNose = () => {
    const { isFixedNose } = this.state
    this.setState({ isFixedNose: !isFixedNose })
    this.checkToggleFixedNose();
  }

  handleToggleFixedWing = () => {
    const { isFixedWing } = this.state
    this.setState({ isFixedWing: !isFixedWing })
    this.checkToggleFixedWing();
  }

  checkToggleFixedBody = () => {
    this.setState({
      isFixedNose : false,
      isFixedWing : false
    })
  }

  checkToggleFixedNose = () => {
    this.setState({
      isFixedBody : false,
      isFixedWing : false
    })
  }

  checkToggleFixedWing = () => {
    this.setState({
      isFixedNose : false,
      isFixedBody : false
    })
  }

  popup_fixed_check = () => {
    this.setState({
      isFixedNose: false,
      isFixedBody : false,
      isFixedWing : false,
    })
  }

  render() {
    const { isFixedBody, isFixedNose, isFixedWing, isMobileMenu, isShowAllFixed } = this.state;

    return (
      <div className={ isMobileMenu ? "tip-tail flex flex-wrap w-full h-full bg-cover relative mobile-active" : "tip-tail flex h-full flex-wrap w-full bg-cover relative"}>
        <div className="w-full flex h-24 justify-between relative z-10">
          <div className="logo p-4 flex items-center h-24">
              <Logo/>
              <h2 className="text-black text-2xl xl:text-6xl italic font-medium leading-none pl-8">Nose to Tail</h2>
          </div>
          <div className="content--inner content"></div>
          <div className="header-right mr-12">
            <a className="hidden button relative xl:inline-flex items-center px-8 button-border-slant text-red-500 text-4xl font-semibold m-3" onClick={this.togglePopup.bind(this)}>Help</a>
            {this.state.showPopup ?  
            <HelpPopup  

              closePopup={this.togglePopup.bind(this)}  
            />  
            : null  
            } 
            <button id="js-mobile" className="uppercase mr-2 menu-toggle menu-toggle--collapse inline-block xl:hidden flex items-center" aria-controls="primary-menu" aria-expanded="false" type="button" onClick={ this.mobileMenu }>
	            <span className="menu-toggle-box ml-2">
			        <span className="menu-toggle-inner"></span>
		          </span>
	          </button>
        </div>
        </div>
        
        <div className="fixed-wrap flex xl:block flex-wrap w-full xl:h-full relative z-10">
          <div className={ isFixedBody || isFixedWing || isFixedNose ? "fixed fixed-active z-0 relative xl:h-full w-full" : "fixed w-full xl:h-full z-0 relative"}>
            <div className="fixed-inner xl:absolute relative">
              <img src="./images/graphics/fixed-wing.png" className="w-full p-12 relative z-10" alt="Passenger Airline Jet"/>
              <button className="marker fixed-nose absolute z-20" className={isFixedNose ? "marker fixed-nose absolute z-20 btn1or2-active" : "marker fixed-nose absolute z-20"} onClick={this.handleToggleFixedNose}>
                <MarkerBtn/>
              </button>

              <button className={isFixedBody ? "marker fixed-body absolute z-20 btn1or2-active" : "marker fixed-body absolute z-20"} onClick={this.handleToggleFixedBody}>
                <MarkerBtn/>
              </button>

              <button className={isFixedWing ? "marker fixed-wing absolute z-20 btn1or2-active" : "marker fixed-wing absolute z-20"} onClick={this.handleToggleFixedWing}>
                <MarkerBtn/>
              </button>
            </div>
          </div> 
          <div>

{ isFixedBody && <FixedBody isOpen={isFixedBody} cls={this.state.cls} close={this.handleToggleFixedBody} /> }
{ isFixedNose && <FixedNose isOpen={isFixedNose} cls={this.state.cls} close={this.handleToggleFixedNose} /> }
{ isFixedWing && <FixedWing isOpen={isFixedWing} cls={this.state.cls} close={this.handleToggleFixedWing} /> }
{isShowAllFixed ?
<ShowAllFixed isOpen = {isShowAllFixed}
close = {this.showAllFixed.bind(this)} />
:null
}
</div>   
        </div>

       

        <div className="bottom-legend fixed bottom-0 left-0 w-1/2 z-30 flex items-end">
        <FixedLegend handleToggleFixedBody={this.handleToggleFixedBody} handleToggleFixedWing={this.handleToggleFixedWing} handleToggleFixedNose={this.handleToggleFixedNose}/>
        </div>
        <div className="hidden xl:block bottom-nav fixed xl:bottom-0 right-0 w-full xl:w-1/2 z-30 flex items-end">
          <div className="flex justify-end w-full relative">
            <FixedNav toggleShowFixed={this.showAllFixed}/>
          </div>
        </div>    
      </div>
      
    );
  }
}

export default TipTail
