import React, { Component } from 'react';
import { Fade } from '../lib';
import LandingNav from '../componets/LandingNav';
import { LazyImageProvider } from "../LazyImage/LazyImageContext";
import LazyImage from "../LazyImage/LazyImage";
import './slider_anim.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      fadeImages: [
        './images/backgrounds/7.jpg',
        './images/backgrounds/15.jpg',
        './images/backgrounds/24.jpg',
        './images/backgrounds/21.jpg',
        './images/backgrounds/4.jpg',
        './images/backgrounds/27.jpg',
        './images/backgrounds/23.jpg',
        './images/backgrounds/17.jpg',
        './images/backgrounds/1.jpg',
        './images/backgrounds/28.jpg',
        './images/backgrounds/20.jpg',
        './images/backgrounds/14.jpg',
        './images/backgrounds/11.jpg',
        './images/backgrounds/8.jpg',
        './images/backgrounds/12.jpg',
        './images/backgrounds/3.jpg',
        './images/backgrounds/18.jpg',
        './images/backgrounds/5.jpg',
        './images/backgrounds/31.jpg',
        './images/backgrounds/22.jpg',
        './images/backgrounds/2.jpg',
        './images/backgrounds/26.jpg',
        './images/backgrounds/34.jpg',
        './images/backgrounds/16.jpg',
        './images/backgrounds/25.jpg',
        './images/backgrounds/9.jpg',
        './images/backgrounds/29.jpg',
        './images/backgrounds/13.jpg', 
        './images/backgrounds/19.jpg',
      ]
    };
  }


  render() {
    

    const fadeProperties = {
      duration: 5000,
      transitionDuration: 500,
      indicators: false,
      arrows: false,
      infinite: true,
    };

 
    const { fadeImages } = this.state;
    const { count } = 0;
    return (
          <div className="slide-container relative bg-white h-full">
            <div className="fixed t-80p w-full z-30">
              <LandingNav/>
            </div>  
            <LazyImageProvider>
              <Fade {...fadeProperties}>
              {fadeImages.map((each, index) => (
                <div key={index} className="each-fade h-full">
                  <div className={"image-container h-full anim-"+ index % 3}>
                    <LazyImage aspectRatio={[10, 7]} src={each} key={index} />
                  </div>
                </div>   

                ))}        
              </Fade>
            </LazyImageProvider>
          </div>
  
    );
  } 
}

export default App;
