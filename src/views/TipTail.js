import React, { Component } from 'react';
import Logo from '../componets/Logo';
import ChopperLegend from '../componets/ChopperLegend';
import ChopperNav from '../componets/ChopperNav';
import HelpPopup from './../componets/HelpPopup';
import RotorPop from '../componets/RotorPop';
import CabinPop from '../componets/CabinPop';
import ChopCpPop from '../componets/ChopCpPop';
import ShowAllRotor from '../componets/ShowAllRotor';


class TipTail extends Component { 
  constructor(props){ 
    super(props);  
    this.state = { 
      cls : "active",
      showPopup: false,
      isToggle: false,
      isToggle1: false,
      isToggle2: false,
      isMobileMenu: false,
      isShowAllRotor: false };
  }  

  togglePopup() {  
    this.setState({  
         showPopup: !this.state.showPopup  
    });  
  } 

  

mobileMenu = () => {
  const { isMobileMenu } = this.state
  this.setState({ isMobileMenu: !isMobileMenu })
}

showAllRotor = () => {
  const { isShowAllRotor } = this.state
  this.setState({ isShowAllRotor: !isShowAllRotor })
  this.popup_rotor_check();
}

 toggle = () => {
    const { isToggle } = this.state
    this.setState({ isToggle: !isToggle })
    this.popup_check();
}
toggle1 = () => {
  const { isToggle1 } = this.state
  this.setState({ isToggle1: !isToggle1 })
  this.popup_check1();
}
toggle2 = () => {
  const { isToggle2 } = this.state
  this.setState({ isToggle2: !isToggle2 })
  this.popup_check2();
}
popup_check = () => {
  this.setState({
    isToggle1 : false,
    isToggle2 : false,
    isShowAllRotor: false
  })
}
popup_check1 = () => {
  this.setState({
    isToggle : false,
    isToggle2 : false,
    isShowAllRotor: false
  })
}
popup_check2 = () => {
  this.setState({
    isToggle1 : false,
    isToggle : false,
    isShowAllRotor: false
  })
}

popup_rotor_check = () => {
  this.setState({
    isToggle: false,
    isToggle1 : false,
    isToggle2 : false,
  })
}

  render() {
    const { isMobileMenu } = this.state;
    const { isToggle } = this.state;
    const { isToggle1 } = this.state;
    const { isToggle2 } = this.state;
    const { isShowAllRotor } = this.state;
    return (
      <div className={ isMobileMenu ? "tip-tail flex flex-wrap w-full h-full bg-cover relative mobile-active" : "tip-tail flex h-full flex-wrap w-full bg-cover relative"}>
        <div className="w-full flex h-24 justify-between relative z-10">
          <div className="logo p-4 flex items-center h-24">
            <Logo/>
            <h2 className="text-black text-2xl xl:text-6xl italic font-medium leading-none pl-8">Tip to Tail</h2>
          </div>
          <div className="content--inner content"></div>
          <div className="header-right mr-12">
            <a className="hidden button relative xl:inline-flex items-center px-8 button-border-slant text-red-500 text-4xl font-semibold m-3" onClick={this.togglePopup.bind(this)}>Help</a>
            {this.state.showPopup ?  
              <HelpPopup   
                closePopup={this.togglePopup.bind(this)}  
              />
              : null  
            } 
            <button id="js-mobile" className="uppercase mr-2 menu-toggle menu-toggle--collapse inline-block xl:hidden flex items-center" aria-controls="primary-menu" aria-expanded="false" type="button" onClick={ this.mobileMenu }>
	            <span className="menu-toggle-box ml-2">
			        <span className="menu-toggle-inner"></span>
		          </span>
	          </button>
        </div>
      </div>
        <div className="heli-wrap w-full xl:h-full relative z-10">
          <div className={ isToggle || isToggle1 || isToggle2 ? "heli heli-active z-0 relative xl:h-full w-full" : "heli w-full xl:h-full z-0 relative"}>
            <div className="heli-inner xl:absolute relative">
              <img src="./images/graphics/heli.png" className="w-full p-12 relative z-10" alt="LORD Helicopter"/>
            
              <button className={isToggle ? "marker rotor-back absolute z-20 btn1or2-active" : "marker rotor-back absolute z-20"} onClick={ this.toggle }>
                <svg xmlns="http://www.w3.org/2000/svg" width="74" height="74" viewBox="0 0 74 74">
                  <g id="Group_1" data-name="Group 1" transform="translate(-475 -281)">
                  <circle id="Ellipse_1" data-name="Ellipse 1" cx="20" cy="20" r="20" transform="translate(492 298)" fill="#ff1200"/>
                  <circle id="Ellipse_2" data-name="Ellipse 2" cx="37" cy="37" r="37" transform="translate(475 281)" fill="#ff1200" opacity="0.432"/>
                  </g>
                </svg>
              </button>
              
              <button className={isToggle ? "marker rotor-front absolute z-20 btn1or2-active" : "marker rotor-front absolute z-20" } onClick={ this.toggle }>
                <svg xmlns="http://www.w3.org/2000/svg" width="74" height="74" viewBox="0 0 74 74">
                <g id="Group_1" data-name="Group 1" transform="translate(-475 -281)">
                <circle id="Ellipse_1" data-name="Ellipse 1" cx="20" cy="20" r="20" transform="translate(492 298)" fill="#ff1200"/>
                <circle id="Ellipse_2" data-name="Ellipse 2" cx="37" cy="37" r="37" transform="translate(475 281)" fill="#ff1200" opacity="0.432"/>
                </g>
                </svg>
              </button>

              <button className={isToggle1 ? "marker chop-side absolute z-20 btn3-active" : "marker chop-side absolute z-20" } onClick={ this.toggle1 }>
                <svg xmlns="http://www.w3.org/2000/svg" width="74" height="74" viewBox="0 0 74 74">
                  <g id="Group_1" data-name="Group 1" transform="translate(-475 -281)">
                  <circle id="Ellipse_1" data-name="Ellipse 1" cx="20" cy="20" r="20" transform="translate(492 298)" fill="#ff1200"/>
                  <circle id="Ellipse_2" data-name="Ellipse 2" cx="37" cy="37" r="37" transform="translate(475 281)" fill="#ff1200" opacity="0.432"/>
                  </g>
                </svg>
              </button>

              <button className={isToggle2 ? "marker chop-nose absolute z-20 btn4-active" : "marker chop-nose absolute z-20" } onClick={ this.toggle2 }>
                <svg xmlns="http://www.w3.org/2000/svg" width="74" height="74" viewBox="0 0 74 74">
                  <g id="Group_1" data-name="Group 1" transform="translate(-475 -281)">
                  <circle id="Ellipse_1" data-name="Ellipse 1" cx="20" cy="20" r="20" transform="translate(492 298)" fill="#ff1200"/>
                  <circle id="Ellipse_2" data-name="Ellipse 2" cx="37" cy="37" r="37" transform="translate(475 281)" fill="#ff1200" opacity="0.432"/>
                  </g>
                </svg>
              </button>
            
            </div>
           
          </div>
          {isToggle ?
          <RotorPop isOpen = {isToggle}
          cls = {this.state.cls}
          close = {this.toggle.bind(this)} />
          :null
          }
          {isToggle1 ?
          <CabinPop isOpen = {isToggle1} 
          cls = {this.state.cls}
          close = {this.toggle1.bind(this)} />
          :null
          }
           {isToggle2 ?
          <ChopCpPop isOpen = {isToggle2}
          cls = {this.state.cls}
          close = {this.toggle2.bind(this)} />
          :null
          }
          {isShowAllRotor ?
          <ShowAllRotor isOpen = {isShowAllRotor}
          close = {this.showAllRotor.bind(this)} />
          :null
          }
        </div>
        <div className="bottom-legend fixed bottom-0 left-0 w-1/2 z-30 flex items-end">
        <ChopperLegend toggle={this.toggle} toggle1={this.toggle1} toggle2={this.toggle2} />
        </div>
        <div className="hidden xl:block bottom-nav fixed xl:bottom-0 right-0 w-full xl:w-1/2 z-30 flex items-end">
          <div className="flex justify-end w-full relative">
            <ChopperNav toggleShowRotor={this.showAllRotor} />
          </div>
        </div>
        
      </div>
    );
  }
}

export default TipTail
