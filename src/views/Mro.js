import React, { Component } from 'react';
import Logo from '../componets/Logo';
import MroNav from '../componets/MroNav.js';
import BasicNav from '../componets/BasicNav';

class mro extends Component {
  render() {
    return (
        <div className="mro w-full h-full bg-cover">
        <div className="w-full flex h-24 justify-between">
          <div className="logo p-4 flex h-24">
            <Logo/>
          </div>
          
          <div className="header-right mr-12">
          <a className="button relative inline-flex items-center px-8 button-border-slant text-red-500 text-4xl font-semibold m-3" href="#">Help</a>
        </div>
        </div>
        <div className="content--inner content"></div>
        <div className="w-full">
          <h2 className="text-black text-6xl italic font-medium leading-none pl-12 pt-8 pb-12">MRO Platforms</h2>
        </div>  
        <div className="mro__wrap w-full flex">
            <div class="w-1/4">
                 <MroNav />
            </div>
            <div class="w-3/4">
              <img src="./images/mro/1-AS350.jpg" alt="AS 350"/>
            </div>
        </div>
        <div className="bottom-nav fixed bottom-0 w-full z-20">
          <div className="flex justify-end w-full relative">
            <BasicNav/>
          </div>
        </div>
      </div>
    );
  }
}

  export default mro; 