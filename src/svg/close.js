import React from 'react';

class CloseBtn extends React.Component {  
    render() {  
  return ( 
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 30 30">
  <g id="noun_cancel_rounded_2318226" data-name="noun_cancel rounded_2318226" transform="translate(-1 -1)">
    <g id="cancel_1" data-name="cancel 1">
      <g id="Group">
        <path id="Path_1" data-name="Path 1" d="M16,31A15,15,0,1,1,31,16,15,15,0,0,1,16,31ZM16,3A13,13,0,1,0,29,16,13,13,0,0,0,16,3Z" fill="#ff1200"/>
      </g>
      <g id="Group-2" data-name="Group">
        <path id="Path_2" data-name="Path 2" d="M11.05,22a1,1,0,0,1-.71-1.71l9.9-9.9a1,1,0,1,1,1.42,1.42l-9.9,9.9A1,1,0,0,1,11.05,22Z" fill="#ff1200"/>
      </g>
      <g id="Group-3" data-name="Group">
        <path id="Path_3" data-name="Path 3" d="M21,22a1,1,0,0,1-.71-.29l-9.9-9.9a1,1,0,0,1,1.42-1.42l9.9,9.9A1,1,0,0,1,21,22Z" fill="#ff1200"/>
      </g>
    </g>
  </g>
</svg>
  )
    }
};

export default CloseBtn;