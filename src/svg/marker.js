import React from 'react';

class MarkerBtn extends React.Component {  
    render() {  
  return ( 

<svg xmlns="http://www.w3.org/2000/svg" width="74" height="74" viewBox="0 0 74 74">
                  <g id="Group_1" data-name="Group 1" transform="translate(-475 -281)">
                  <circle id="Ellipse_1" data-name="Ellipse 1" cx="20" cy="20" r="20" transform="translate(492 298)" fill="#ff1200"/>
    <circle id="Ellipse_2" data-name="Ellipse 2" cx="37" cy="37" r="37" transform="translate(475 281)" fill="#ff1200" opacity="0.432"/>
  </g>
</svg>

  )
    }
};

export default MarkerBtn;