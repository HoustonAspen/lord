import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class RotorPop extends React.Component {  
    render() {  
      const { isOpen } = this.props;
  return ( 
    <CSSTransition
          in = {isOpen}
          appear = {true}
          timeout = {600}
          classNames = "fade"
          > 
        <div className={"productModal " + this.props.cls}>
            <div className="p-8 xl:w-1/2 xl:absolute grid-1 top-0 left-0 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-xl xl:text-3xl leading-none px-8 pb-4 pt-8 leading-tight">Blade Retention and Motion Accommodation</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={6}
        visibleSlides={window.innerWidth < 1000 ? 1 : 3}
      >
          <Slider>
          <Slide index={0}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Spherical Bearing</p></Slide>
          <Slide index={1}><img src="./images/rotor-modal/369D21721-21-Tail-Rotor-Fork-Assembly_High-Res_6196.jpg" alt="Tail-Rotor-Fork-Assembly"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tail Rotor Fork Assembly</p></Slide>
          <Slide index={2}><img src="./images/rotor-modal/Tension-Torsion-Straps_High-Res_9949.jpg" alt="Tension-Torsion-Straps_High-Res"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tension Torsion Strap</p></Slide>
          <Slide index={3}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing-CA.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">HCL Elastomeric Bearing</p></Slide>
          <Slide index={4}><img src="./images/rotor-modal/Main-Rotor-Focal-Bearing.jpg" alt="Main-Rotor-Focal-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Focal Bearing</p></Slide>
          <Slide index={5}><img src="./images/rotor-modal/Main-Rotor-Pitch-Link-Rod-End.jpg" alt="Main-Rotor-Pitch-Link-Rod-End"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Pitch Link Rod End</p></Slide>
          </Slider>
        <ButtonBack>Back</ButtonBack>
        <ButtonNext>Next</ButtonNext>

      </CarouselProvider>
      </div>
      <div className="p-8">
                    <p className="text-lg pb-4">LORD pioneered the incorporation of elastomeric bearings in helicopter rotor systems.The bearings are designed to react blade CF load and accommodate blade motions associated with feathering, lead-lag and flap.</p>

                    <p className="text-lg">“Elastomeric rotor hubs” are low/no maintenance, do not require grease or lubricants, provide vibration isolation, and have slow degradation mode that is easily monitored by visual inspection.</p>
</div>
                </div>
                </div>
                <div className="p-8 xl:w-1/2 xl:absolute top-0 right-0 grid-2 z-30 relative">               
                    <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                        <h3 className="font-bold text-xl xl:text-3xl px-8 pb-4 pt-8 leading-tight">Lead Lag Dampers</h3>
                        <div className="sliderBG px-12 relative">
                        <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={4}
        visibleSlides={window.innerWidth < 1000 ? 1 : 3}
      >
           <Slider>
          <Slide index={0}><div className="w-full flex justify-center"><img src="./images/rotor-modal/AW169-rotary-fluid-damper.jpg" alt="otary-fluid-damper"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper (Rotary)</p></Slide>
          <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/FL-1036-4-Fluidlastic-Lead-Lag-Damper_Office_6338.jpg" alt="Fluidlastic-Lead-Lag-Damper_Office"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper</p></Slide>
          <Slide index={2}><div className="w-full flex justify-center"><img src="./images/rotor-modal/J-15640-6-Main-Rotor-Lead-Lag-Damper_High-Res_7764.jpg" alt="Main-Rotor-Lead-Lag-Damper"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Elastomeric Lead Lag Damper</p></Slide>
          <Slide index={3}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Main-Rotor-Hub-Damper-Blue-Coated-Isolated_High-Res_7856.jpg" alt="Hub-Damper-Blue-Coated-Isolated"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Elastomeric Lead Lag Damper</p></Slide>
          </Slider>
        <ButtonBack>Back</ButtonBack>
        <ButtonNext>Next</ButtonNext>

      </CarouselProvider>
      </div>
      <div className="p-8 bottom-text">
      <p className="text-lg pb-4">Lead-lag dampers are used to control the motions of rotor blades thereby ensuring smooth and stable operation.</p>
      <p className="text-lg">LORD’s elastomeric and Fluidlastic designs are low/no maintenance, self-contained (no adding fluid or pressurization), and adaptable to various hub designs.</p>
      </div>
                    </div>
                </div>

                <div className="p-8 xl:absolute right-0 bottom-0 xl:w-1/2 grid-3 z-30 relative mb-24">
                    <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-xl xl:text-3xl px-8 pb-4 pt-8 leading-tight">Rotor Hub Assembly</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={2}
        visibleSlides={window.innerWidth < 1000 ? 1 : 3}
      >
          <Slider>
          <Slide index={0}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Rotor-Hub-Assembly-for-Bell-407-Isolated_High-Res_8703.jpg" alt="otary-fluid-damper"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rotor Hub Assembly for Bell 407</p></Slide>
          <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Rotor-Hub-for-Bell-429---Isolated_Office_9614.jpg" alt="Rotor-Hub-for-Bell-429"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rotor Hub Assembly for Bell 429</p></Slide>
         </Slider>
    

      </CarouselProvider>
      </div>
      <div className="p-8">
      <p className="text-lg">LORD has extensive in-house and supply chain capability to manufacture critical machined components and provide the rotor hub as a completed assembly. </p>
      </div>
                    </div>
                </div>

                <div className="red-1 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
                <div className="red-2 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
           

            </div>
            </CSSTransition>
  )
    }
}

    export default RotorPop;