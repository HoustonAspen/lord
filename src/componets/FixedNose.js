import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class FixedNose extends React.Component {  
    render() {  
      const { isOpen } = this.props;
  return ( 
    <CSSTransition
          in = {isOpen}
          appear = {true}
          timeout = {600}
          classNames = "fade"
          > 
        <div className={"productModal " + this.props.cls}>
            <div className="p-8 xl:p-24 xl:w-1/2 xl:absolute grid-10 top-0 right-0 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                  <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Cockpit Controls</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                          naturalSlideWidth={100}
                          naturalSlideHeight={125}
                          totalSlides={6}
                          visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                        >
                          <Slider>
                            <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/RPA trans bkgrnd.jpg" alt="Main-Rotor-Spherical-Bearing"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rudder Pedal Assembly</p></Slide>
                            <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Throttle-Control-Unit-Black-Label_High-Res_11242.jpg" alt="Throttle-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Throttle Control</p></Slide>
                            <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Nose-Wheel-Steering-Control-Unit_High-Res_11246.jpg" alt="Nose-Wheel-Steering-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Nose Wheel Steering Control</p></Slide>
                            <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Flap-Lever-Control-Unit_High-Res_11247.jpg" alt="Flap-Lever-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Flap Lever Control</p></Slide>
                            <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Speed-Brake-321200_High-Res_10851.jpg" alt="Speed-Brake-321200_High-Res"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Speed Brake</p></Slide>
                            <Slide index={5}><div className="w-full flex justify-center"><img src="./images/fixed-modal/SIDE STICK CONTROLLER 3012 q.jpg" alt="SIDE STICK CONTROLLER"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Slide Stick Controller</p></Slide>
                          </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                    <div className="p-8">
                      <p className="text-lg pb-4">LORD cockpit controls and inceptors provide functional and ergonomic interfaces between pilots and various aircraft fly-by-wire systems (flight control, engine control, landing gear). Flight control inceptors can be tailored to sidestick or yoke based layouts.</p>
                    </div>
                </div>
              </div>
              <div className="red-10 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
            </div>
          </CSSTransition>
  )
    }
}

    export default FixedNose;