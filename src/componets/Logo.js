import React from 'react';
import { NavLink } from 'react-router-dom';

const routes = [
  { to: '/', label: 'Home' },
];

const Nav = () => {
  const links = routes.map(({ to, label }) => {
    return <NavLink strict exact to={to} key={to}><img src="./images/graphics/lord-logo.png" className="w-full" alt="LORD Logo"/></NavLink>}
  )

  return (<div className="w-48 lg:w-auto">
    { links }
  </div>
  );
}

export default Nav
