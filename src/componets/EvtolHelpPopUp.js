import React from 'react';
import CloseBtn from '../svg/close';

class EvtolHelpPopUp extends React.Component {  
    render() {  
  return ( 
    <div className='popup flex items-center justify-center'>   
    <div className="border-4 border-red-500 bg-white w-1/3 relative p-12">
    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.closePopup}><CloseBtn/></button>
      <div className="box-content">
        <h3 className="text-center leading-tight pb-8">Welcome to LORD's eVTOL Products.</h3>
        <div className="">
          <p className="pb-4 text-xl">Click / Touch any of th ebuttons on the left to learn more about the LORD products available in those areas.</p>
          <p className="pb-4 text-xl">Touch the <span className='span'>LORD logo</span> in the upper left corner to return to the home screen at any time.</p>
        </div>
      </div> 
      {/*<div className="learn"><a  href="#">Learn More</a></div> */}
    </div> 
    </div>
  );  
  }  
  }  
  
  export default EvtolHelpPopUp;