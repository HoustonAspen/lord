import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class CabinPop extends React.Component {  
    render() {  
        const { isOpen } = this.props;
  return ( 
    <CSSTransition
    in = {isOpen}
    appear = {true}
    timeout = {600}
    classNames = "fade"
    > 
        <div className={"productModal " + this.props.cls}>
            <div className="xl:pt-24 xl:w-1/2 xl:absolute grid-4 top-0 left-0 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Pylon and Engine Mounting Systems</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/429-310-201-105-J-23823-12-Pitch-Restraint-Spring_High-Res_9144.jpg" alt="Pitch-Restraint-Spring_High"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pitch Restraint</p></Slide>
                                <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Live-Mount_High-Res_8896.jpg" alt="Tail-Rotor-Fork-Assembly"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Live Mount</p></Slide>
                                <Slide index={2}><img src="./images/rotor-modal/Pylon-Isolator_High-Res_5953.jpg" alt="Pylon-Isolator"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pylon Isolator</p></Slide>
                                <Slide index={3}><img src="./images/rotor-modal/Bell-Tail-Rotor-Bearing_High-Res_7593.jpg" alt="Bell-Tail-Rotor-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Corner Mount</p></Slide>
                                <Slide index={4}><img src="./images/rotor-modal/Fluid-Mount_High-Res_10685.jpg" alt="Fluid-Mount"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pylon Fluid Isolator</p></Slide>
                                <Slide index={5}><img src="./images/rotor-modal/fluid-torque-restraint.jpg" alt="fluid-torque-restraint"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">LORD Fluid Torque Restraint</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>

                        </CarouselProvider>
                    </div>
                    <div className="p-8">
                        <p className="text-lg">LORD Pylon Mounts securely attach the rotor and transmission to the helicopter while reducing vibration transmitted to the cabin. LORD Engine Mounting systems capabilities include hard mounts as well as elastomeric and Fluidlastic mounting systems.  Our solutions often include full structures including yokes and attachments.</p>
                    </div>
                </div>
            </div>

            <div className="xl:w-1/2 xl:absolute right-0 grid-5 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                        <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Equipment and Cabin Interiors Isolators</h3>
                        <div className="sliderBG px-12 relative">
                            <CarouselProvider
                                naturalSlideWidth={100}
                                naturalSlideHeight={125}
                                totalSlides={2}
                                visibleSlides={2}
                            >
                                <Slider>
                                    <Slide index={0}><img src="./images/rotor-modal/broad-temperature-Range-Avionics-Mounts.jpg" alt="broad-temperature-Range-Avionics-Mounts"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper (Rotary)</p></Slide>
                                    <Slide index={1}><img src="./images/rotor-modal/LORD-Miniature-Mounts-Collage_Original_9723.jpg" alt="LORD-Miniature-Mounts-Collage_Original"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper</p></Slide>
                                </Slider>
                            </CarouselProvider>
                        </div>
                        <div className="p-8 bottom-text">
                            <p className="text-lg pb-4">LORD equipment isolators set the standard for compact, high-load, high-capacity vibration isolation mounts. Our vibration isolators are designed to support and protect avionics equipment in all types of aircraft and defense systems.
                            </p>
                            <p className="text-lg">LORD offers a full suite of interiors isolators, vibration isolation mounts and elastomeric rod ends for nearly every interiors mounting application.  Our engineering staff that can work with an aircraft OEM to achieve better cabin acoustics. </p>
                        </div>
                </div>
            </div>

            <div className="red-3 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
        </div>
    </CSSTransition>
  )
    }
}

    export default CabinPop;