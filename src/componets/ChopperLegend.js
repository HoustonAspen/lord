import React, { Component } from 'react';

class ChopperLegend extends Component {

    state = {
        isToggle: false,
        lastToggle: undefined
    }

    toggle = () => {
        const { isToggle } = this.state
        this.setState({ isToggle: !isToggle })
    }

    showPopup = (popup) => {
        this.toggle();
        if(this.state.lastToggle !== popup) {
            this.props[popup]();
            this.setState({
                lastToggle: popup
            })
        }
    }

    render() {

        const { isToggle } = this.state

        return (
            <div
                id="c-legend"
                className={
                    isToggle ?
                    "c-legend tip-tail-leg z-20 absolute left-0 active"
                    :
                    "c-legend tip-tail-leg z-20 absolute left-0"
                }
            >
                <button
                    id="btn-slant"
                    className="button-slant uppercase relative text-white tracking-widest font-semibold text-3xl pr-6 pl-3 z-20"
                    onClick={ this.toggle }
                >
                    <span className="relative z-30">Legend</span>
                </button>
                <div className="c-legend__slantbg bg-gray-200 absolute z-40 shadow"></div>
                <ul className="c-legend__menu relative z-50 pt-8 uppercase leading-none">
                    <li><a onClick={() => {this.showPopup('toggle2'); }}>Active Vibration Control</a></li>
                    <li><a onClick={() => {this.showPopup('toggle'); }}>Aftermarket / MRO</a></li>
                    <li><a onClick={() => {this.showPopup('toggle'); }}>Blade Retention and Motion Accommodation</a></li>
                    <li><a onClick={() => {this.showPopup('toggle2'); }}>Cockpit Controls</a></li>
                    <li><a onClick={() => {this.showPopup('toggle2'); }}>Electromechanical Actuation</a></li>
                    <li><a onClick={() => {this.showPopup('toggle1'); }}>Equipment and Cabin Isolators</a></li>
                    <li><a onClick={() => {this.showPopup('toggle'); }}>Lead Lag Dampers</a></li>
                    <li><a onClick={() => {this.showPopup('toggle1'); }}>Pylon and Engine Mounting Systems</a></li>
                    <li><a onClick={() => {this.showPopup('toggle'); }}>Rotor Hub Assembly</a></li>
                    <li><a onClick={() => {this.showPopup('toggle2'); }}>Sensing Systems</a></li>
                </ul>

            </div>
        );
    }
}

  
export default ChopperLegend


