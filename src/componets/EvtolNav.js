import React, { Component } from 'react';

class EvtolNav extends Component {
  constructor(props) {
    super(props);
    this.state = {activeClasses: [true, false, false, false, false, false, false, false, false, false, false, false, false, false, false]};
    this.changepic= this.changepic.bind(this);
  }
  changepic = (e) => {
    let a = e.currentTarget.getAttribute('id'); 
    let { activeClasses } = this.state;
    this.props.pic(a);
    let newClasses = activeClasses.map((_, idx) => idx === +a ? true : false);

    this.setState({
      activeClasses: newClasses
    });
  }
    render() {
      const activeClasses = this.state.activeClasses.slice();
      return (
        <div className="mro__menu pl-20">
            <ul>
            <li className={activeClasses[0]? "current" : ""}><span className="rexe absolute"></span><a id="0" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 z-10 leading-none text-center"><span className="relative z-30">Electric VTOL Solutions</span></a></li>
            <li className={activeClasses[1]? "current" : ""}><span className="rexe absolute"></span><a id="1" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Overview</span></a></li>
            <li className={activeClasses[2]? "current" : ""}><span className="rexe absolute"></span><a id="2" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Overview Cont.</span></a></li>
            <li className={activeClasses[3]? "current" : ""}><span className="rexe absolute"></span><a id="3" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Cockpit Controls</span></a></li>
            <li className={activeClasses[4]? "current" : ""}><span className="rexe absolute"></span><a id="4" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Electromechanical Actuation</span></a></li>
            <li className={activeClasses[5]? "current" : ""}><span className="rexe absolute"></span><a id="5" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Electromechanical Actuation Cont.</span></a></li>
            <li className={activeClasses[6]? "current" : ""}><span className="rexe absolute"></span><a id="6" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Sensors</span></a></li>
            <li className={activeClasses[7]? "current" : ""}><span className="rexe absolute"></span><a id="7" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Isolators</span></a></li>
            <li className={activeClasses[8]? "current" : ""}><span className="rexe absolute"></span><a id="8" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Cabin Comfort</span></a></li>
            <li className={activeClasses[9]? "current" : ""}><span className="rexe absolute"></span><a id="9" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Blade Attachment</span></a></li>
            <li className={activeClasses[10]? "current" : ""}><span className="rexe absolute"></span><a id="10" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Gimbaled Rotor Systems</span></a></li>
            <li className={activeClasses[11]? "current" : ""}><span className="rexe absolute"></span><a id="11" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Thermal Management Materials</span></a></li>
            <li className={activeClasses[12]? "current" : ""}><span className="rexe absolute"></span><a id="12" onClick={this.changepic} className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-lg lg:text-xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Moving Beyond Concept to Reality</span></a></li>
            </ul>
        </div>
      )
    }
}

export default EvtolNav;