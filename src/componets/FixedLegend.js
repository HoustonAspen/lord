import React, { Component } from 'react';

class FixedLegend extends Component {

    state = {
        isToggle: false
    }

    toggle = () => {
        const { isToggle } = this.state
        this.setState({ isToggle: !isToggle })
    }

    showPopup = (popup) => {
        this.toggle();
        if(this.state.lastToggle !== popup) {
            this.props[popup]();
            this.setState({
                lastToggle: popup
            })
        }
    }

    render() {

        const { isToggle } = this.state

        return (
            <div
                id="c-legend"
                className={
                    isToggle ?
                    "c-legend nose-tail-leg z-20 absolute left-0 active"
                    :
                    "c-legend nose-tail-leg z-20 absolute left-0"
                }
            >
                <button
                    id="btn-slant"
                    className="button-slant uppercase relative text-white tracking-widest font-semibold text-3xl pr-6 pl-3 z-20"
                    onClick={ this.toggle }
                >
                    <span className="relative z-30">Legend</span>
                </button>
                <div className="c-legend__slantbg bg-gray-200 absolute z-40 shadow"></div>
                <ul className="c-legend__menu relative z-50 pt-8 uppercase leading-none">
                    <li><a onClick={() => {this.showPopup('handleToggleFixedNose'); }}>Cockpit Controls</a></li>
                    <li><a onClick={() => {this.showPopup('handleToggleFixedBody'); }}>Actuation</a></li>
                    <li><a onClick={() => {this.showPopup('handleToggleFixedWing'); }}>Mounts & Isolators</a></li>
                </ul>

            </div>
        );
    }
}

  
export default FixedLegend


