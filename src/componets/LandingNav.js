import React from 'react';
import { NavLink } from 'react-router-dom';

const routes = [
  { to: '/tip-to-tail', label: 'Tip To Tail' },
  { to: '/nose-to-tail', label: 'Nose to Tail' }
];

const Nav = () => {
  const links = routes.map(({ to, label }) => {
    return <li key={to} className="relative">
      <span className="lex absolute"></span>
      <span className="rex absolute"></span>
      <NavLink strict exact to={to} key={to}><span>{label}</span></NavLink></li>}
  )

  return <nav className="main-nav"><ul className="flex flex-col md:flex-row items-center justify-center">{ links }</ul></nav>;
}

export default Nav
