import React from 'react';
import { NavLink } from 'react-router-dom';



const FixedNav = ({ toggleShowFixed }) => {

  return ( 
  
  <div className="bottomNav w-full lg:w-1/2 lg:mr-16">
      <div className="bottomNav__top flex justify-center lg:justify-end mb-4">
      <NavLink to="/electric-hybrid-vertical-take-off-landing-solutions" className="button-slant flex items-center justify-center relative text-white tracking-widest font-semibold w-48 text-2xl pr-6 pl-3"><span className="relative z-30">eVTOL</span></NavLink>
      
      </div>
      <div className="bottomNav__top flex flex-col lg:flex-row items-center justify-center lg:justify-end mb-4">
      <button to="/" className="button-slant uppercase relative text-white mb-4 lg:mb-0 tracking-widest font-semibold text-2xl w-48 pr-6 pl-3 lg:mr-20 z-20 leading-none" onClick={toggleShowFixed}><span className="relative z-30">Show All</span></button>
      <NavLink to="/tip-to-tail" className="button-slant uppercase flex items-center justify-center text-center relative text-white tracking-widest font-semibold w-48 text-2xl pr-6 pl-3 leading-none z-20"><span className="relative z-30">Rotary Wing</span></NavLink>
      {/*<NavLink to="/mro" className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 w-48 pl-3 mr-20">MRO</NavLink>
      <button to="/" className="button-slant uppercase relative text-white tracking-widest font-semibold text-xl pr-6 w-48 pl-3 leading-none z-20"><span className="relative z-30">Request Follow-Up</span></button>*/}
      </div>
  </div>

  );
}

export default FixedNav
