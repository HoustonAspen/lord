import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class FixedWing extends React.Component {  
    render() {  
      const { isOpen } = this.props;
  return ( 
    <CSSTransition
          in = {isOpen}
          appear = {true}
          timeout = {600}
          classNames = "fade"
          > 
        <div className={"productModal " + this.props.cls}>
            <div className="p-8 xl:p-24 xl:w-1/2 xl:absolute grid-11 right-0 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                  <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Mounts & Isolators</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                          naturalSlideWidth={100}
                          naturalSlideHeight={125}
                          totalSlides={5}
                          visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                        >                        
                          <Slider>
                            <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Engine Attachment System-Aft.jpg" alt="Engine Attachment System-Aft"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Engine Attachment System (Aft)</p></Slide>
                            <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Engine Attachment System-Forward.jpg" alt="Engine Attachment System-Forward"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Engine Attachment System (Forward)</p></Slide>
                            <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/custom mounting solution.jpg" alt="custom mounting solution"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Auviliary Power Unit</p></Slide>
                            <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/broad temperature Range Avionics Mounts trans.jpg" alt="broad temperature Range Avionics Mounts trans"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Broad Remperature Range Avionics Mounts</p></Slide>
                            <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/LORD Miniature Mounts Collage_Original_9723 trans.jpg" alt="LORD Miniature Mounts Collage"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Miniature Mounts</p></Slide>
                          </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                    <div className="p-8">
                      <p className="text-lg">LORD Corporation aerospace equipment vibration isolators set the standard for compact, high-load, high-capacity vibration isolation mounts. Our vibration isolators are designed to support and protect avionics equipment in all types of aircraft and defense systems.</p>
                    </div>
                </div>
              </div>
              <div className="red-12 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
              <div className="red-13 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
              <div className="red-14 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
            </div>
          </CSSTransition>
  )
    }
}

    export default FixedWing;