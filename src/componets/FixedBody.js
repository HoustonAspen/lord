import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class FixedBody extends React.Component {  
    render() {  
      const { isOpen } = this.props;
  return ( 
    <CSSTransition
          in = {isOpen}
          appear = {true}
          timeout = {600}
          classNames = "fade"
          > 
        <div className={"productModal " + this.props.cls}>
            <div className="p-8 xl:w-1/2 xl:absolute grid-11 right-0 z-30 relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                  <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Actuation</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                          naturalSlideWidth={100}
                          naturalSlideHeight={125}
                          totalSlides={5}
                          visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                        >                        
                          <Slider>
                            <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/417 790 AILERON TRIM TAB 220000.jpg" alt="Main-Rotor-Spherical-Bearing"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Aileron Trim Tab</p></Slide>
                            <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/425 650 RUDDER TRIM ACTUATOR 731300 droite.jpg" alt="Throttle-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rudder Trim Actuator</p></Slide>
                            <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Releveur porte Sesame-Door Lifter_High Res_17263 trans.jpg" alt="Nose-Wheel-Steering-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Door Actuator</p></Slide>
                            <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/question - cockpit controlslinear actuator.jpg" alt="cockpit controlslinear actuator"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Linear Actuator</p></Slide>
                            <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/question - compact linear actuator trans.jpg" alt="compact linear actuator trans.jpg"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">APU Door Actuator</p></Slide>
                          </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                    <div className="p-8">
                      <p className="text-lg">LORD provides electromechanical actuators (EMA) for both flight control and utility applications, including applications where hydraulic actuation is prevalent, such as spoilers and landing gear operations.</p> 
                    </div>
                </div>
              </div>
              <div className="red-11 bg-red-500 opacity-75 absolute border-4 border-red-500 z-20" onClick={this.props.close}></div>
            </div>
          </CSSTransition>
  )
    }
}

    export default FixedBody;