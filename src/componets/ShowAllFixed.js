import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class ShowAllFixed extends React.Component {  
    render() {  
        const { isOpen } = this.props;
  return ( 
    <CSSTransition
    in = {isOpen}
    appear = {true}
    timeout = {600}
    classNames = "fade"
    > 
        <div className="productModal absolute top-0 w-full left-0 z-50">
            <div className="xl:flex">
                <div className="p-8 xl:p-24 xl:w-1/2 z-30 relative">
                    <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                        <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                        <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">CockPit Controls</h3>
                        <div className="sliderBG px-12 relative">
                            <CarouselProvider
                                naturalSlideWidth={100}
                                naturalSlideHeight={125}
                                totalSlides={6}
                                visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                            >
                                <Slider>
                                <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/RPA trans bkgrnd.jpg" alt="Main-Rotor-Spherical-Bearing"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rudder Pedal Assembly</p></Slide>
                                <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Throttle-Control-Unit-Black-Label_High-Res_11242.jpg" alt="Throttle-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Throttle Control</p></Slide>
                                <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Nose-Wheel-Steering-Control-Unit_High-Res_11246.jpg" alt="Nose-Wheel-Steering-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Nose Wheel Steering Control</p></Slide>
                                <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Flap-Lever-Control-Unit_High-Res_11247.jpg" alt="Flap-Lever-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Flap Lever Control</p></Slide>
                                <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Speed-Brake-321200_High-Res_10851.jpg" alt="Speed-Brake-321200_High-Res"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Speed Brake</p></Slide>
                                <Slide index={5}><div className="w-full flex justify-center"><img src="./images/fixed-modal/SIDE STICK CONTROLLER 3012 q.jpg" alt="SIDE STICK CONTROLLER"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Slide Stick Controller</p></Slide>
                                </Slider>
                                <ButtonBack>Back</ButtonBack>
                                <ButtonNext>Next</ButtonNext>
                            </CarouselProvider>
                        </div>
                    </div>
                </div>

                <div className="p-8 xl:p-24 xl:w-1/2 z-30 relative">
                    <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                        <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                        <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Actuation</h3>
                        <div className="sliderBG px-12 relative">
                            <CarouselProvider
                                naturalSlideWidth={100}
                                naturalSlideHeight={125}
                                totalSlides={5}
                                visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                            >                        
                                <Slider>
                                    <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/417 790 AILERON TRIM TAB 220000.jpg" alt="Main-Rotor-Spherical-Bearing"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Aileron Trim Tab</p></Slide>
                                    <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/425 650 RUDDER TRIM ACTUATOR 731300 droite.jpg" alt="Throttle-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rudder Trim Actuator</p></Slide>
                                    <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Releveur porte Sesame-Door Lifter_High Res_17263 trans.jpg" alt="Nose-Wheel-Steering-Control-Unit"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Door Actuator</p></Slide>
                                    <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/question - cockpit controlslinear actuator.jpg" alt="cockpit controlslinear actuator"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Linear Actuator</p></Slide>
                                    <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/question - compact linear actuator trans.jpg" alt="compact linear actuator trans.jpg"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">APU Door Actuator</p></Slide>
                                </Slider>
                                <ButtonBack>Back</ButtonBack>
                                <ButtonNext>Next</ButtonNext>
                            </CarouselProvider>
                        </div>
                    </div>
                </div>
            </div>  

            <div className="xl:flex justify-center">
                <div className="p-8 xl:p-24 xl:w-1/2 z-30 relative">
                    <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                        <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                        <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Mounts & Isolators</h3>
                        <div className="sliderBG px-12 relative">
                            <CarouselProvider
                                naturalSlideWidth={100}
                                naturalSlideHeight={125}
                                totalSlides={5}
                                visibleSlides={window.innerWidth < 1000 ? 1 : 2}
                            >                        
                                <Slider>
                                <Slide index={0}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Engine Attachment System-Aft.jpg" alt="Engine Attachment System-Aft"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Engine Attachment System (Aft)</p></Slide>
                                <Slide index={1}><div className="w-full flex justify-center"><img src="./images/fixed-modal/Engine Attachment System-Forward.jpg" alt="Engine Attachment System-Forward"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Engine Attachment System (Forward)</p></Slide>
                                <Slide index={2}><div className="w-full flex justify-center"><img src="./images/fixed-modal/custom mounting solution.jpg" alt="custom mounting solution"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Auviliary Power Unit</p></Slide>
                                <Slide index={3}><div className="w-full flex justify-center"><img src="./images/fixed-modal/broad temperature Range Avionics Mounts trans.jpg" alt="broad temperature Range Avionics Mounts trans"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Broad Remperature Range Avionics Mounts</p></Slide>
                                <Slide index={4}><div className="w-full flex justify-center"><img src="./images/fixed-modal/LORD Miniature Mounts Collage_Original_9723 trans.jpg" alt="LORD Miniature Mounts Collage"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Miniature Mounts</p></Slide>
                                </Slider>
                                <ButtonBack>Back</ButtonBack>
                                <ButtonNext>Next</ButtonNext>
                            </CarouselProvider>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </CSSTransition>
  )
    }
}

    export default ShowAllFixed;