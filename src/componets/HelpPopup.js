import React from 'react';
import CloseBtn from '../svg/close';

class Popup extends React.Component {  
    render() {  
  return ( 
    <div className='popup flex items-center justify-center'>   
    <div className="border-4 border-red-500 bg-white w-1/3 relative p-12">
    <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.closePopup}><CloseBtn/></button>
      <div className="box-content">
        <h3 className="text-center leading-tight pb-8">Welcome to LORD's Aerospace Interactive.</h3>
        <div className="">
          <p className="pb-4 text-xl">Touch anywhere on the aircart to learn more about the LORD products available for those areas. You can also use the <span className='span'>Legend</span> in the bottom left.</p>
          <p className="pb-4 text-xl">To view specific products available for different plaforms, touch the <span className='span'>"eVTOL"</span> button at the bottom right of the screen.</p>
          <p className="pb-4 text-xl">Touch the <span className='span'>Fixed/Rotary Wing</span> button to navigate to that aircraft.</p>
          <p className="pb-4 text-xl">Touch the <span className='span'>LORD logo</span> in the upper left corner to return to the home screen at any time.</p>
        </div>
      </div> 
      {/*<div className="learn"><a  href="#">Learn More</a></div> */}
    </div> 
    </div>
  );  
  }  
  }  
  
  export default Popup;