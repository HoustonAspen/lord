import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { CSSTransition } from 'react-transition-group';
import 'pure-react-carousel/dist/react-carousel.es.css';
import CloseBtn from '../svg/close';

class ShowAllRotor extends React.Component {  
    render() {  
        const { isOpen } = this.props;
  return ( 
    <CSSTransition
    in = {isOpen}
    appear = {true}
    timeout = {600}
    classNames = "fade"
    > 
        <div className="productModal ShowAll absolute top-0 w-full left-0 z-50">

            <div>
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Blade Retention and Motion Accommodation</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Spherical Bearing</p></Slide>
                                <Slide index={1}><img src="./images/rotor-modal/369D21721-21-Tail-Rotor-Fork-Assembly_High-Res_6196.jpg" alt="Tail-Rotor-Fork-Assembly"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tail Rotor Fork Assembly</p></Slide>
                                <Slide index={2}><img src="./images/rotor-modal/Tension-Torsion-Straps_High-Res_9949.jpg" alt="Tension-Torsion-Straps_High-Res"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tension Torsion Strap</p></Slide>
                                <Slide index={3}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing-CA.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">HCL Elastomeric Bearing</p></Slide>
                                <Slide index={4}><img src="./images/rotor-modal/Main-Rotor-Focal-Bearing.jpg" alt="Main-Rotor-Focal-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Focal Bearing</p></Slide>
                                <Slide index={5}><img src="./images/rotor-modal/Main-Rotor-Pitch-Link-Rod-End.jpg" alt="Main-Rotor-Pitch-Link-Rod-End"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Pitch Link Rod End</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>

            <div>               
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Lead Lag Dampers</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={4}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/AW169-rotary-fluid-damper.jpg" alt="otary-fluid-damper"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper (Rotary)</p></Slide>
                                <Slide index={1}><img src="./images/rotor-modal/FL-1036-4-Fluidlastic-Lead-Lag-Damper_Office_6338.jpg" alt="Fluidlastic-Lead-Lag-Damper_Office"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper</p></Slide>
                                <Slide index={2}><img src="./images/rotor-modal/J-15640-6-Main-Rotor-Lead-Lag-Damper_High-Res_7764.jpg" alt="Main-Rotor-Lead-Lag-Damper"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Elastomeric Lead Lag Damper</p></Slide>
                                <Slide index={3}><img src="./images/rotor-modal/Main-Rotor-Hub-Damper-Blue-Coated-Isolated_High-Res_7856.jpg" alt="Hub-Damper-Blue-Coated-Isolated"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Elastomeric Lead Lag Damper</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>

            <div>
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Rotor Hub Assembly</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={2}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/Rotor-Hub-Assembly-for-Bell-407-Isolated_High-Res_8703.jpg" alt="otary-fluid-damper"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rotor Hub Assembly for Bell 407</p></Slide>
                                <Slide index={1}><img src="./images/rotor-modal/Rotor-Hub-for-Bell-429---Isolated_Office_9614.jpg" alt="Rotor-Hub-for-Bell-429"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Rotor Hub Assembly for Bell 429</p></Slide>
                            </Slider>
                        </CarouselProvider>
                    </div>
                </div>
            </div>    

            <div className="relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Active Vibration Control</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                              <Slide index={0}><img src="./images/rotor-modal/210mm-CFG_Office_11266.jpg" alt="210mm-CFG_Office_11266"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Central Controller</p></Slide>
                              <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Circular-Force-Generators-Office-7817.jpg" alt="AVCS-154mm-Circular-Force-Generators_Office"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Circular Force Generators</p></Slide>
                              <Slide index={2}><img src="./images/rotor-modal/AVCS-Accelerometers_Office_7831.jpg" alt="AVCS-Accelerometers_Office"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Accelerometer</p></Slide>
                              <Slide index={3}><img src="./images/rotor-modal/IVCS_Office_10428.jpg" alt="IVCS_Office"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Circular Force Generator (CH-47)</p></Slide>
                              <Slide index={4}><img src="./images/rotor-modal/Linear-Force-Generator.jpg" alt="Linear-Force-Generator"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Linear Force Generator</p></Slide>
                              <Slide index={5}><img src="./images/rotor-modal/LAVCS-System-for-EC-130-T-2_Office_7827.jpg" alt="LAVCS-System-for-EC-130-T-2_Office"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">LORD Active Vibration Control Systems</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>

            <div className="relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Cockpit Controls</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={4}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/cockpit-controlslinear-actuator.jpg" alt="cockpit-controlslinear-actuator"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Linear Actuator</p></Slide>
                                <Slide index={1}><img src="./images/rotor-modal/cockpit-controls---side-stick-controller.jpg" alt="cockpit-controls---side-stick-controller"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Side Stick Controler</p></Slide>
                                <Slide index={3}><img src="./images/rotor-modal/cockpit-controls---force-feel-trim-unit.jpg" alt="cockpit-controls---force-feel-trim-unit"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Force Feel TRIM Unit</p></Slide>
                                <Slide index={4}><img src="./images/rotor-modal/cockpit-controls---cyclic-pitch-control-damper-.jpg" alt="cockpit-controls---cyclic-pitch-control-damper"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Cyclic Pitch Control Damper</p></Slide>
                           
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>

            <div className="relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Electromechanical Actuation</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={4}
                            visibleSlides={2}
                        >
                            <Slider>
                                <Slide index={0}><div className="w-full flex justify-center"><img src="./images/rotor-modal/417-790-AILERON-TRIM-TAB-220000.jpg" alt="417-790-AILERON-TRIM-TAB"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Trim Actuator</p></Slide> 
                                <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/cockpit-controlslinear-actuator.jpg" alt="cockpit-controlslinear-actuator"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Linear Actuator</p></Slide>
                                <Slide index={2}><div className="w-full flex justify-center"><img src="./images/rotor-modal/ElectromechanicalActuator.jpg" alt="Electromechanical Actuator"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper</p></Slide>
                                <Slide index={3}><div className="w-full flex justify-center"><img src="./images/rotor-modal/425-650-RUDDER-TRIM-ACTUATOR-731300-droite.jpg" alt="425-650-RUDDER-TRIM-ACTUATOR-731300-droite"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper (Rotary)</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>

            <div className="relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Sensing Systems</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={2}
                            visibleSlides={2}
                        >
                            <Slider>
                                <Slide index={0}><div class="w-full flex justify-center"><img src="./images/rotor-modal/Microstrain-Inertial-Navigation-Sensor.jpg" alt="broad-temperature-Range-Avionics-Mounts"/></div><p className="text-center text-lg leading-tight text-red-500 font-semibold px-4">Microstrain Inertial Navigation Sensor</p></Slide>
                                <Slide index={1}><div class="w-full flex justify-center"><img src="./images/rotor-modal/quadraplex.jpg" alt="LORD-Miniature-Mounts-Collage_Original"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Quadraplex Position Sensor</p></Slide>
                            </Slider>
                        </CarouselProvider>
                    </div>                   
                </div>
            </div>    

            <div className="relative">
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl leading-none px-8 pb-4 pt-8">Pylon and Engine Mounting Systems</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/429-310-201-105-J-23823-12-Pitch-Restraint-Spring_High-Res_9144.jpg" alt="Pitch-Restraint-Spring_High"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pitch Restraint</p></Slide>
                                <Slide index={1}><div className="w-full flex justify-center"><img src="./images/rotor-modal/Live-Mount_High-Res_8896.jpg" alt="Tail-Rotor-Fork-Assembly"/></div><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Live Mount</p></Slide>
                                <Slide index={2}><img src="./images/rotor-modal/Pylon-Isolator_High-Res_5953.jpg" alt="Pylon-Isolator"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pylon Isolator</p></Slide>
                                <Slide index={3}><img src="./images/rotor-modal/Bell-Tail-Rotor-Bearing_High-Res_7593.jpg" alt="Bell-Tail-Rotor-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Corner Mount</p></Slide>
                                <Slide index={4}><img src="./images/rotor-modal/Fluid-Mount_High-Res_10685.jpg" alt="Fluid-Mount"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Pylon Fluid Isolator</p></Slide>
                                <Slide index={5}><img src="./images/rotor-modal/fluid-torque-restraint.jpg" alt="fluid-torque-restraint"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">LORD Fluid Torque Restraint</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div>
            </div>
                
            <div>
                <div className="border-4 border-red-500 bg-gray-100 m-5 relative">
                <button className="close absolute top-0 right-0 text-red-500 text-3xl m-2" onClick={this.props.close}><CloseBtn/></button>
                    <h3 className="font-bold text-3xl px-8 pb-4 pt-8">Equipment and Cabin Interiors Isolators</h3>
                    <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={2}
                            visibleSlides={2}
                        >
                            <Slider>
                                <Slide index={0}><img src="./images/rotor-modal/broad-temperature-Range-Avionics-Mounts.jpg" alt="broad-temperature-Range-Avionics-Mounts"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper (Rotary)</p></Slide>
                                <Slide index={1}><img src="./images/rotor-modal/LORD-Miniature-Mounts-Collage_Original_9723.jpg" alt="LORD-Miniature-Mounts-Collage_Original"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Fluidlastic Lead Lag Damper</p></Slide>
                            </Slider>
                        </CarouselProvider>
                    </div>
                </div>
            </div>
        </div>
    </CSSTransition>
  )
    }
}

    export default ShowAllRotor;