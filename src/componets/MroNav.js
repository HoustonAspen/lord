import React, { Component } from 'react';

class MroNav extends Component {
    render() {
      return (
        <div className="mro__menu pl-20">
            <ul>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4 z-10"><span className="relative z-30">AS350</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4 z-10 leading-none text-center"><span className="relative z-30">Bell 204, 205, 212</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 206</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 214</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 222</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 230</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 407</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 412</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 412</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 427</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 429</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">Bell 430</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4"><span className="relative z-30">MD500</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Schweizer 300</span></a></li>
                <li><a className="button-slant uppercase flex items-center justify-center relative text-white tracking-widest font-semibold text-2xl pr-6 pl-3 mr-20 mb-4 leading-none text-center"><span className="relative z-30">Schweizer 330</span></a></li>

            </ul>
        </div>
      )
    }
}

export default MroNav;