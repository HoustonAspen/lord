import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Transition, TransitionGroup } from 'react-transition-group';
import { play, exit } from './timelines';
import MainLanding from './views/MainLanding';
import NoseTail from './views/NoseTail';
import TipTail from './views/TipTail';
import Evtol from './views/Evtol';
import mro from './views/Mro';

class App extends Component {
	render() {
		return (
			<BrowserRouter basename={'/demo/lordv12'}>
				<div className='app h-screen w-full'>
					<Route
						render={({ location }) => {
							const { pathname, key } = location;

							return (
								<TransitionGroup component={null}>
									<Transition
										key={key}
										appear={true}
										onEnter={(node, appears) => play(pathname, node, appears)}
										onExit={(node, appears) => exit(node, appears)}
										timeout={{ enter: 750, exit: 150 }}>
										<Switch location={location}>
												<Route exact path='/' component={MainLanding} />
												<Route path='/nose-to-tail' component={NoseTail} />
												<Route path='/tip-to-tail' component={TipTail} />
												<Route
													path='/electric-hybrid-vertical-take-off-landing-solutions'
													component={Evtol}
												/>
												<Route path='/mro' component={mro} />
												<Route path='*' component={MainLanding} />
										</Switch>
									</Transition>
								</TransitionGroup>
							);
						}}
					/>
				</div>
			</BrowserRouter>
		);
	}
}

export default App;
