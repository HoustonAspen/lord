import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

class ChopNose extends Component{
    state ={ 
        slideImages : [
        'images/slide1.png',
        'images/slide1.png',
        'images/slide1.png',
        'images/slide1.png'
      ]
    }
    render() {
        const properties = {
            duration: 5000,
            transitionDuration: 500,
            infinite: true,
            indicators: true,
            arrows: true,
            onChange: (oldIndex, newIndex) => {
              console.log(`slide transition from ${oldIndex} to ${newIndex}`);
            }
        }
        const { slideImages } = this.state;
        const { isOpen } = this.props;
        return(
            <CSSTransition
          in = {isOpen}
          appear = {true}
          timeout = {600}
          classNames = "fade"
          > 
          <div className='popup'>
            <div className="container_pop">
            <div className="close"> <button onClick={this.props.close}>X</button> </div>
                <div className="popup_box">
                    <div className="popup_box-centent">
                        <div className="heading_box">
                            <h3>Blade Retention and motion accommodation</h3>
                        </div>
                        <div className="content_peragraph">
                        <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                            <Slide index={0}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Spherical Bearing</p></Slide>
                            <Slide index={1}><img src="./images/rotor-modal/369D21721-21-Tail-Rotor-Fork-Assembly_High-Res_6196.jpg" alt="Tail-Rotor-Fork-Assembly"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tail Rotor Fork Assembly</p></Slide>
                            <Slide index={2}><img src="./images/rotor-modal/Tension-Torsion-Straps_High-Res_9949.jpg" alt="Tension-Torsion-Straps_High-Res"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tension Torsion Strap</p></Slide>
                            <Slide index={3}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing-CA.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">HCL Elastomeric Bearing</p></Slide>
                            <Slide index={4}><img src="./images/rotor-modal/Main-Rotor-Focal-Bearing.jpg" alt="Main-Rotor-Focal-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Focal Bearing</p></Slide>
                            <Slide index={5}><img src="./images/rotor-modal/Main-Rotor-Pitch-Link-Rod-End.jpg" alt="Main-Rotor-Pitch-Link-Rod-End"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Pitch Link Rod End</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>

                        </CarouselProvider>
                        </div>
                        <div className="contentg">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum    has been the industry's standard dummy text ever</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s dummy text of the printing Lorem Ipsum has been the industry's standard.</p>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="popup_box second_box">
                    <div className="popup_box-centent">
                         <div className="heading_box">
                            <h3>Lead Lag Dampers</h3>
                        </div>
                        <div className="content_peragraph">
                            <ul>
                                <li>image</li>
                                <li>image</li>
                                <li>image</li>
                            </ul>
                        </div>
                        <div className="contentg">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum    has been the industry's standard dummy text ever</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s dummy text of the printing Lorem Ipsum has been the industry's standard.</p>
                        </div>
                    </div>
                </div>
                <div className="main_box">
                <div className="popup_box third_box">
                    <div className="popup_box-centent">
                         <div className="heading_box">
                            <h3>Lead Lag Dampers</h3>
                        </div>
                        <div className="content_peragraph">
                            <ul>
                                <li>image</li>
                                <li>image</li>
                                <li>image</li>
                            </ul>
                        </div>
                        <div className="contentg">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum    has been the industry's standard dummy text ever</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s dummy text of the printing Lorem Ipsum has been the industry's standard.</p>
                        </div>
                    </div>
                </div>
                </div>
                <div className="popup_box fourth_popup">
                    <div className="popup_box-centent">
                         <div className="heading_box">
                            <h3>Lead Lag Dampers</h3>
                        </div>
                        <div className="sliderBG px-12 relative">
                        <CarouselProvider
                            naturalSlideWidth={100}
                            naturalSlideHeight={125}
                            totalSlides={6}
                            visibleSlides={window.innerWidth < 1000 ? 1 : 3}
                        >
                            <Slider>
                            <Slide index={0}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Spherical Bearing</p></Slide>
                            <Slide index={1}><img src="./images/rotor-modal/369D21721-21-Tail-Rotor-Fork-Assembly_High-Res_6196.jpg" alt="Tail-Rotor-Fork-Assembly"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tail Rotor Fork Assembly</p></Slide>
                            <Slide index={2}><img src="./images/rotor-modal/Tension-Torsion-Straps_High-Res_9949.jpg" alt="Tension-Torsion-Straps_High-Res"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Tension Torsion Strap</p></Slide>
                            <Slide index={3}><img src="./images/rotor-modal/Main-Rotor-Spherical-Bearing-CA.jpg" alt="Main-Rotor-Spherical-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">HCL Elastomeric Bearing</p></Slide>
                            <Slide index={4}><img src="./images/rotor-modal/Main-Rotor-Focal-Bearing.jpg" alt="Main-Rotor-Focal-Bearing"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Focal Bearing</p></Slide>
                            <Slide index={5}><img src="./images/rotor-modal/Main-Rotor-Pitch-Link-Rod-End.jpg" alt="Main-Rotor-Pitch-Link-Rod-End"/><p className="text-center text-xl leading-tight text-red-500 font-semibold px-4">Main Rotor Pitch Link Rod End</p></Slide>
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>

                        </CarouselProvider>
                        </div>
                        <div className="contentg">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum    has been the industry's standard dummy text ever</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s dummy text of the printing Lorem Ipsum has been the industry's standard.</p>
                        </div>
                    </div>
                </div>
                </div>
                </div>
               </CSSTransition>
          );
      }
  }
  export default ChopNose